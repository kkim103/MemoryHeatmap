import numpy as np
from sklearn.decomposition import PCA
import pandas as pd
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
from sklearn.mixture import GaussianMixture as GM

class GMMforEM:
    # should fit when the constructor is called
    def __init__(self, EM, mixture_count, threshold):
        self.EM = EM
        self.predictor = GM(n_components=mixture_count)
        self.predictor.fit(self.EM)
        self.threshold = threshold

    def predict(self, x):
        return self.predictor.score_samples(x)

    def find_threshold(self):
        score = self.predictor.score_samples(self.EM)
        score = np.sort(score)
        print(score)
        return np.percentile(score, self.threshold)


class EigenMemory:
    def __init__(self, dataset, L_prime_size):
        self.M = self._getEigenMemory(dataset, L_prime_size)

    # M := L by N matrix where N is the size of the dataset
    def _getEigenMemory(self,M, L_prime_size):
        self.covariance = np.cov(M, rowvar=False)
        self.pca = PCA(n_components=L_prime_size)
        self.pca.fit(self.covariance)
        return self.pca.transform(M)

    def transform(self, X):
        return self.pca.transform(X)

##### code from stackoverflow #####
def shuffle_in_unison(a, b):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
##### end code #####


###### the code below are from another website ########
# set this to true for sanity checking
if False:

    url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
    # load dataset into Pandas DataFrame
    df = pd.read_csv(url, names=['sepal length','sepal width','petal length','petal width','target'])


    features = ['sepal length', 'sepal width', 'petal length', 'petal width']
    # Separating out the features
    x = df.loc[:, features].values
    # Separating out the target
    y = df.loc[:,['target']].values

    ### edited by kkim ###
    # leave last quarter for testing
    dataset_size = len(x)
    last_quarter =  -1 * int(dataset_size * 0.25)
    shuffle_in_unison(x,y)
    x_test = x[last_quarter:]
    y_test = y[last_quarter:]
    x = x[:last_quarter]
    y = y[:last_quarter]
    print(x_test.shape)
    print(y_test.shape)
    ### end edit ###

    EM = EigenMemory(x, 2)
    principalComponents = EM.M

    principalDf = pd.DataFrame(data = principalComponents, columns = ['pc 1', 'pc 2'])
    labelDf = pd.DataFrame(data=y, columns=['target'])
    finalDf = pd.concat([principalDf, labelDf], axis = 1)


    fig = plt.figure(figsize = (8,8))
    ax = fig.add_subplot(1,1,1)
    ax.set_xlabel('Principal Component 1', fontsize = 15)
    ax.set_ylabel('Principal Component 2', fontsize = 15)
    ax.set_title('2 component PCA', fontsize = 20)
    targets = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']
    colors = ['r', 'g', 'b']
    for target, color in zip(targets,colors):
        indicesToKeep = finalDf['target'] == target
        ax.scatter(finalDf.loc[indicesToKeep, 'pc 1']
                   , finalDf.loc[indicesToKeep, 'pc 2']
                   , c = color
                   , s = 50)
    ax.legend(targets)
    ax.grid()

    plt.show()


    gm_model = GMMforEM(x,3,1)
    print(gm_model.predict(x_test))
    print(y_test)
    print(gm_model.find_threshold())


    ### kkim TODO:
    ###     test to visually see which training data point is the most anomalous
    ###     Create data handler (partitioning for trianing)
