#!/bin/bash

# first argument determines if the qemu needs to be traced or debugged
CHARDEV="-chardev socket,path=/tmp/foo,server,nowait,id=foo"
SERIAL="-serial chardev:foo"
TRACE=""

BASE_DIR=~/Workspace/research/files/DiskImage
DISK_IMG=$BASE_DIR/disks/gentoo.img
KERN=$BASE_DIR/vmlinuz-4.9.0-7-rt-amd64
INITRD=$BASE_DIR/initrd.img-4.9.0-7-rt-amd64

if [ -z "$1" ]; then
	SERIAL="-serial mon:stdio"
	qemu-system-x86_64 -m 512 -hda $DISK_IMG -nographic \
		-kernel $KERN -initrd $INITRD \
		-append 'console=ttyS0 root=/dev/sda1 rw noaslr' \
        $SERIAL
    echo "not-recording"
    exit 1
fi

if [ $1 -gt 1 ]; then
	TRACE="-trace events=./events.txt"
elif [ $1 -lt 1 ]; then
    CHARDEV=""
	SERIAL="-serial mon:stdio"
fi




if [ $1 -lt 1 ]; then
	qemu-system-x86_64 -m 512 -hda $DISK_IMG -nographic \
		-kernel $KERN -initrd $INITRD \
		-append 'console=ttyS0 root=/dev/sda1 rw noaslr init=/bin/bash' \
        $SERIAL
    echo "not-recording"
else
    for i in `seq 1 $2`;
    do
        qemu-system-x86_64 -m 512 -hda $DISK_IMG -nographic \
            -kernel $KERN -initrd $INITRD \
            -append 'console=ttyS0 root=/dev/sda1 rw noaslr init=/bin/bash' \
            $CHARDEV \
            $SERIAL \
            $TRACE \
            &> /dev/null &
        python3 read-socket.py /tmp/foo >> ./timestamps.txt
        killall qemu-system-x86_64 &> /dev/null
    done
fi
