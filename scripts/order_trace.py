import glob
import os
import subprocess
import sys

def parse_attr(attr_list):
    attr_dict = {}
    for attr in attr_list:
        part = attr.split("=")
        attr_dict[part[0].strip()] = part[-1].strip()
    return attr_dict

# parses simple_traced  row
def parse_srow(row, attr_of_interest=None):
    output = {}
    row = row.split(" ")
    output["time"] = float(row[1])
    attr_dict = parse_attr(row[2:])
    if attr_of_interest == None:
        output = output.update(attr_dict)
    else:
        for aoi in attr_of_interest:
            output[aoi]=attr_dict[aoi]
    return output


# TODO: it only outputs addr value
# parse and trim the trace after the cutoff
# parse_dict["log type"]["row attribute"]
# parse_dict["checkpoint"] := contains the name of the row attribute to look for.
def mem_trim_trace(input_file, output_file, trim_addr, parse_dict):
    # cirtual duration of the remaining trace (in units equal to qemu)
    start_recording = False
    record_vdur=0
    with open(output_file, "w") as of:
        with open(input_file) as f:
            traces = f.readlines()
            for log in traces:
                row_type = log.split(" ")[0]
                if row_type in parse_dict:
                    attrs = parse_dict[row_type]
                    row_dict = parse_srow(log, attrs)
                    checkpoint_attr = parse_dict["checkpoint"]
                    if checkpoint_attr in attrs:
                        addr = row_dict[checkpoint_attr]
                        # cutoff_time is in seconds, ts is in nano seconds
                        if addr == trim_addr:
                            print("FOUND CHECKPOINT ")
                            start_recording = True
                    if start_recording:
                        record_vdur += row_dict["time"]
                        of.write(log)
    print("Remaining trace total time: " + str(record_vdur))


count = 0
specific_step = False
step_num = 0
if len(sys.argv) > 1:
    step_num = int(sys.argv[-1])

def extract_number(name):
    return name.split("-")[-1]

simpletrace_file = "~/Workspace/"

trace_list = glob.glob("./trace-*")
trace_list = sorted(list(map(extract_number, trace_list)))
# start renaming the files
i = 0

# check if the timestmps.txt has equal number of time stamps as the one
with open("./timestamps.txt") as f:
    checkpoint_pcs = f.readlines()

checkpoint_pcs = [int(x.strip(), 16) for x in checkpoint_pcs]
if len(checkpoint_pcs) != len(trace_list):
    print("number of timestamps and trace_list not matching")
    exit()

for tn in trace_list:
    i+=1

    # Step 0: initialize all necessary files paths
    home_dir = "/home/kkim"
    raw_trace = home_dir + "/Workspace/MemoryHeatmap/scripts/trace-"+str(tn)
    parsed_trace=home_dir + "/Workspace/MemoryHeatmap/scripts/TRACE/parsed_"+str(i)+".log"
    simplescript =home_dir +  "/Workspace/qemu-git/scripts/simpletrace.py"
    event_file = home_dir + "/Workspace/qemu-git/trace-events-tb"
    timestamp_script = home_dir + "/Workspace/MemoryHeatmap/scripts/abs_timestamp.pl"
    timestamped_trace = home_dir + "/Workspace/MemoryHeatmap/scripts/TRACE/timestamped_"+str(i)+".log"
    trimmed_trace = home_dir + "/Workspace/MemoryHeatmap/scripts/TRACE/trimmed_"+str(i)+".log"

    if (specific_step and step_num == 1) or not specific_step:
        # Step 1: run simple script
        with open(parsed_trace, "w") as f:
            subprocess.call([simplescript, event_file, raw_trace] , stdout=f)

    '''
    if (specific_step and step_num == 2) or not specific_step:
        # Step 2: run the perl script to timestamp the trace
        with open(timestamped_trace, "w") as f:
            subprocess.call(["perl","-w", timestamp_script, parsed_trace], stdout=f)
    '''
    if (specific_step and step_num == 3) or not specific_step:
        # Step 3: trim the part of the trace that is not needed
        checkpoint_pc = checkpoint_pcs[i-1] # since i starts with 1 not 0
        parse_dict = {}
        parse_dict["memory_region_ops_read"]=["mr", "addr","value","size"]
        parse_dict["memory_region_ops_write"]=["mr", "addr","value","size"]
        parse_dict["memory_region_tb_read"]=["addr","value","size"]
        parse_dict["memory_region_tb_write"]=["addr","value","size"]
        parse_dict["checkpoint"] = "addr"
        mem_trim_trace(parsed_trace, trimmed_trace, checkpoint_pc, parse_dict)


