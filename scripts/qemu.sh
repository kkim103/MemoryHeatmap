#!/usr/bin/env python3
import subprocess as sp
import re
import logging
import argparse
import glob
import signal
import json
import os
import sys

BASE_DIR = "~/Workspace/research/files/DiskImage"
DRIVE = BASE_DIR+"/disks/gentoo.img"
KERN = BASE_DIR+"/vmlinuz-4.9.0-7-rt-amd64"
QEMU = "qemu-system-x86_64"
enable_kvm = False

parser = argparse.ArgumentParser()
parser.add_argument(
    '-d',
    '--debug',
    help="Print lots of debugging statements",
    action="store_const",
    dest="loglevel",
    const=logging.DEBUG,
    default=logging.INFO,
)
parser.add_argument(
    '-r',
    '--record',
    help="record the trace",
    type=bool
)
parser.add_argument(
    '-m',
    '--manual',
    help="manual access to the shell",
    type=bool
)

args = parser.parse_args()

logging.basicConfig(
    level=args.loglevel,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

qemu_logger = logging.getLogger("qemu")

if !args.record:
    print("logs muted")
    qemu_logger.disable()

def sigint_handler(sig, frame):
    kill_qemu()
    sys.exit(0)


signal.signal(signal.SIGINT, sigint_handler)


## FUNCTIONS
def kill_qemu():
    cp = sp.run(["pkill", "qemu"])
    if cp.returncode == 0:
        qemu_logger.debug("qemu is killed")
    if cp.returncode == 1:
        qemu_logger.info("no running qemu found")


def run_qemu(args):
    # TODO: edit this line
    return

    final_cmd = [QEMU]
    for a in args:
        final_cmd.append(a)
        final_cmd.append(args[a])

    final_cmd.append("-nographic")

    qemu_logger.debug(final_cmd)

    with sp.Popen(final_cmd, stdout=sp.PIPE, stdin=sp.PIPE) as proc:
        output = []
        for l in proc.stdout:
            qemu_logger.debug(l.decode('ascii')[:-1])
            if b"[ end Kernel panic - " in l:
                proc.kill()
            output.append(l)

    return output


def get_config_name(path):
    return os.path.basename(path[:path.rindex("/")])


def bench(kernel):
    logger = logging.getLogger(args.bench)
    result = []

    append = "console=ttyS0 root=/dev/sda rw init=/root/bench/{}.sh".format(
        args.bench)
    qemu_args = {
        "-m": "4G",
        "-cpu": "host",
        "-kernel": kernel,
        "-drive": DRIVE,
        "-smp": str(args.smp),
        "-append": append
    }

    for i in range(1, args.iter + 1):
        output = run_qemu(qemu_args)
        result.append(b''.join(output).decode('ascii'))
    return result
## END


if __name__ == "__main__":
    result = {}

    logging.info("Number of kernels: " + str(len(args.kernels)))
    logging.debug(args.kernels)

    for kernel in args.kernels:
        config = get_config_name(kernel)
        bench_logger = logging.getLogger(config)
        result[config] = []

        if not os.path.exists(kernel):
            bench_logger.critical("{} not found".format(kernel))
            continue

        bench_logger.info("Benchmarking {}".format(config))

        result[config] = bench(kernel)

    if args.output:
        with open(args.output, 'w') as f:
            f.write(json.dumps(result, sort_keys=True, indent=4))
            f.close()
