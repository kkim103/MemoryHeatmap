import os 
import glob
from subprocess import call

class TraceParser:
    def __init__(self, parse_script, event_file, timestamp_script):
        self.parse_script=parse_script
        self.event_file=event_file
        self.timestamp_script=timestamp_script

    def parse(self, raw_trace, output):
        cmd = "python3 "+self.parse_script+" "+self.event_file+" "+raw_trace+" > "+output
        call(cmd, shell=True)

    def timestamp(self, parsed_trace, output):
        cmd = "perl -w "+self.timestamp_script+" "+parsed_trace+" > "+output
        call(cmd, shell=True)

    def run(self, raw_trace, parsed_trace, output):
        self.parse(raw_trace, parsed_trace)
        self.timestamp(parsed_trace, output)

if __name__ == "__main__":

    tag = ""
    if len(os.argv) < 2 || len(os.argv) > 3:
        print("requires one or two arguments")
        exit()
    elif len(os.argv) == 3:
        tag = os.argv[1]
    ps="~/Documents/ext/qemu-3.0.0/scripts/simpletrace.py"
    ef="~/Documents/ext/qemu-3.0.0/trace-events-all"
    ts="~/Documents/Git/MemoryHeatmap/scripts/abs_timestamp.pl"
    parser = TraceParser(ps,ef,ts)
    index = 0
    for trace in glob.glob("./trace-*"):
        parser.run(trace, "trace/parsed_"tag+str(index)+".log","trace/timestamp_"tag+str(index)+".log")
        index += 1
    
