import sys
import numpy as np
import math
import io
import matplotlib.pylab as plt
from matplotlib.animation import FuncAnimation
import seaborn as sns
import imageio
import os
from sklearn.cluster import DBSCAN


# MemTraceCollection parses and filters uneeded parts of the memory using DBSCAN
# eps is used in filter stage. it is the max distance to consider a neighbour
# min_samples is the minimum points needed to form a cluster
class MemTraceCollection:
    def __init__(self, filename, time_delta, eps, min_samples, bin_size):
        trace = self._parse(filename)
        chunks = self._chunk_addr(trace, time_delta)
        filtered_chunks = self._filter(chunks, eps, min_samples)
        self.mem_traces=[]
        for c in filtered_chunks:
            mhm = MemTrace(c, bin_size)
            self.mem_traces.append(mhm)
        print("number of clusters: " + str(len(self.mem_traces)))

    # private function that returns a list of dicts
    def _parse(self, filename):
        trace = []
        with open(filename, "r") as tlog:
            for line in tlog:
                parts = line.split(" ")
                duration = float(parts[0].strip())
                pc = int(parts[1].strip(),16)
                trace.append((duration, pc))
        return trace

    # create a chunk of memory used for mem heatmap by binning the history by time intervals
    # if the addr list does not completely fill the time_delta, then don't creat the chunk
    def _chunk_addr(self, trace_list, time_delta):
        chunk_list = []
        cumulative_time = 0
        chunk = []
        for addr_tuple in trace_list:
            duration = addr_tuple[0]
            addr = addr_tuple[1]
            if duration + cumulative_time > time_delta :
                cumulative_time = 0
                chunk_list.append(chunk)
                chunk = []
            cumulative_time += duration
            chunk.append((duration, addr))
        return chunk_list

    def _filter(self, chunks, eps, min_samples):
        # TODO: Filter the unneeded part of the memory
        X = []
        for outer_it in range(len(chunks)):
            for index in range(len(chunks[outer_it])):
                # trace_list is composed of tuples (duration, pc_addr)
                X.append(chunks[outer_it][index])
        db = DBSCAN(eps=eps, min_samples=min_samples).fit(X)

        # get the number of clusters
        label_cardinality = {}
        for l in db.labels_:
            if l == -1:
                continue
            label_cardinality[l] = 1
        label_cardinality = len(label_cardinality)

        print(label_cardinality)

        # create chunk_list for each cluster
        # mhm_clusters[chunk count][labels]
        mhm_clusters = []

        # create one chunk for each mhm
        label_it = 0
        for outer_it in range(len(chunks)):
            mhm_cluster.append([])
            for c in range(label_cardinality):
                mhm_cluster[-1].append([])

            for index in range(len(chunks[outer_it])):
                label = db.labels_[label_it]
                label_it += 1

                if label != -1:
                    mhm_cluster[-1][label].append(chunks[output_it][index])
        return mhm_clusters


# MemTrace is a representation of MHM
# mini_chunks are chunked traces from the DBSCAN cluster
class MemTrace:
    def __init__(self, mini_chunks, bin_size):
        self.mhms = _create_mhm(mini_chunks, bin_size)

    # FAR FUTURE TODO: allow weigh each access by time by user discretion
    # No longer requires distinction between kernel and userspace
    def _create_mhm(self, chunks, bin_size):
        highest_addr = 0
        lowest_addr = math.inf

        # scan for the lowest addr and highest
        for chunk in chunks:
            for pairs in chunk:
                pc = int(pairs[1], 16)
                if pc < lowest_addr:
                    lowest_addr = pc
                if pc > highest_addr:
                    highest_addr = pc

        addr_range = addr_highest_addr - addr_lowest_addr
        bin_count = math.ceil(addr_range / bin_size)
        mhms = []

        for chunk in chunks:
            addr_mhm = np.zeros(bin_count)
            for pairs in chunk:
                pc = int(pairs[1], 16)
                adjusted_pc = pc - lowest_addr
                mhm_index = math.floor(adjusted_pc / bin_size)
                if mhm_index >= bin_count:
                    mhm_index = addr_mhm - 1
                addr_mhm[mhm_index] += 1
            mhms.append(addr_mhm)
        return mhms

class TraceStat:
    # filename is the name of the timestamped trace file
    def __init__(self, mhm_trace):
        self.mhm_trace = mhm_trace
        self.vmax = self._get_vmax(self.mhm_trace)

    def visualize(self,width, output_name):
        self._visualize(self.mhm_trace, width, output_name, self.vmax)

    def animate(self,width, output_name):
        self._animate(self.mhm_trace, width, output_name, self.vmax)

    # visualize the memory heatmap
    # TODO: normalize the heatmap visualization by finding the max number of bin accesses
    # assume square mhm
    def _visualize(self, mhm, width, output_name, vmax):
        mhm_reshaped = np.reshape(mhm, (width, -1))
        ax = sns.heatmap(mhm_reshaped,
                linewidth=1,
                vmin = 0,
                vmax = vmax,
                cmap = sns.cm.rocket_r,
                xticklabels=[],
                yticklabels=[])
        with open(output_name, "w"):
            plt.savefig(output_name, format="png")
        plt.clf()

    def _get_vmax(self, mhm_list):
        vmax = 0
        for mhm in mhm_list:
            for v in mhm:
                if vmax < v:
                    vmax = v

    def _animate(self, mhm_list, width, out_dir):
        images = []
        vmax = get_vmax(mhm_list)
        for index in range(len(mhm_list)):
            name = out_dir + "/" +str(index)+".png"
            visualize(mhm_list[index], width, name, vmax)
            images.append(imageio.imread(name))
        imageio.mimsave("./animation.gif", images, duration = 0.02)

def print_total_accesses(mhm_list):
    total = []
    for mhm in mhm_list:
        total.append(np.sum(mhm))
    return total

def count_empty_mhm(mhm_list):
    total = 0
    for mhm in mhm_list:
        if np.sum(mhm) == 0:
            total += 1
    return total


# return a stat of a particular memory segment
# the following stats are returned: median address and standard deviation
def memory_stat(addr_list):
    median = np.median(addr_list)
    std = np.std(addr_list)
    return median, std

def unique_count(addr_list):
    # check number of unique addresses
    unique_set = set()
    with open(sys.argv[1], "r") as tlog:
        for line in tlog:
            parts = line.split(" ")
            unique_set.add(pc)
        print(len(unique_set))

    return None


# TODO: Get finer detail of the memory heatmap

if __name__ == "__main__":
    # check if the number of arguments is correct
    mhm_bin_size = 2024

    if len(sys.argv) != 2:
        print("invalid number of arguments\n")
        print("current argv count is {0}\n".format(len(sys.argv)))
        exit()

    # check number of unique addresses
    unique_set = set()
    with open(sys.argv[1], "r") as tlog:
        for line in tlog:
            parts = line.split(" ")
            timestamp = float(parts[0])
            pc = parts[1]

            unique_set.add(pc)

        #print(len(unique_set))

    #10,000 = 10 ms
    stats = MemTraceCollection(sys.argv[-1], 10000, 100, 20, mhm_bin_size)
    #print(len(stats.usr_mhm))
    #animate(stats.kern_mhm, mhm_bin_size, "./images")
    #print(count_empty_mhm(stats.usr_mhm))
    #print(count_empty_mhm(stats.kern_mhm))
    # print(stats.get_chunk()[0])
