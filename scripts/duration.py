import sys

if len(sys.argv) != 2:
    ValueError("Invalid number of argument. Need trimmed_log file name as argument")

total_time = 0
with open(sys.argv[-1]) as log_file:
    content = log_file.readlines()
    for log_line in content:
        time = float(log_line.split(" ")[0].strip())
        total_time += time
print(total_time)



