import socket
import sys
import os, stat


start_string= None
checkpoint_addr=None

if len(sys.argv) != 2:
    print("invalid number of argument. Specify socket file")
    exit()

mode = os.stat(sys.argv[1]).st_mode

while not stat.S_ISSOCK(mode):
    continue

sock = socket.socket(socket.AF_UNIX)


sock.connect(sys.argv[1])
string_buf=""
read_string=''

trigger_string = "bash: no job control in this shell\r"
finish_string = "END TASK\r"


while True:
    data = sock.recv(1024)
    if not data: break
    read_string = data.decode('iso8859')
    string_buf += read_string
    #print(read_string, end='')
    #print("recieved")

    newline = '\n'
    nindex = string_buf.find(newline)

    while nindex != -1:
        pstring=string_buf[:nindex]
        #//print(pstring)
        if "MALLOC_ADDR:" in pstring:
            #//print("FOUND CHECKPOINT STRING")
            checkpoint_addr = pstring.split(" ")[-1].strip()
        elif "PC_ADDR" in pstring:
            pc_addr = pstring.split(" ")[-1].strip()
            print(pstring)
        elif trigger_string in pstring:
            #//print("TRIGGERED")
            sock.send("/home/g/rt-task.sh\n".encode("iso8859"))
        elif finish_string in pstring:
            print("MALLOC_ADDR: " + checkpoint_addr)
            exit()

        string_buf=string_buf[nindex+len(newline):]
        nindex = string_buf.find(newline)
